const express = require("express");
const router = express.Router();
const utils = require("../utils");
router.get("/:name", (request, response) => {
  const { name } = request.params;

  const connection = utils.openConnection();

  const statement = `
        select * from Movie where
        name = '${name}'
      `;
  connection.query(statement, (error, result) => {
    connection.end();
    if (result.length > 0) {
      console.log(result.length);
      console.log(result);
      response.send(utils.createResult(error, result));
    } else {
      response.send("Movie not found !");
    }
  });
});

router.post("/add", (request, response) => {
  const { name, title,release_date, time,director} = request.body;

  const connection = utils.openConnection();
  const statement = `
        insert into Movie
          ( default, 'name', 'title','release_date', 'time','director')
        values
          ( '${name}','${title}','${release_date}','${time}','${director}')
      `;
  connection.query(statement, (error, result) => {
    connection.end();
    response.send(utils.createResult(error, result));
  });
});

router.put("/update/:name", (request, response) => {
  const { name } = request.params;
  const { release_date } = request.body;
  const { time } = request.body;


  const statement = `
    update Movie
    set
    Movie_Release_Date ='${release_date}'
    Movie_Time ='${time}'

    where
      name = '${name}'
  `;
  const connection = utils.openConnection();
  connection.query(statement, (error, result) => {
    connection.end();
    console.log(statement);

    response.send(utils.createResult(error, result));
  });
});


router.delete("/remove/:name", (request, response) => {
  const { name } = request.params;
  const statement = `
    delete from Movie
    where
      name = '${name}'
  `;
  const connection = utils.openConnection();
  connection.query(statement, (error, result) => {
    connection.end();
    response.send(utils.createResult(error, result));
  });
});
module.exports = router;
